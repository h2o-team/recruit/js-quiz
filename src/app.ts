import dateFormatter from "./lib/dateFormatter"

class Quiz {
  public say = (): Quiz => {
      const timeToFormat = "20191002101020"
      const octopusBirthday = dateFormatter.getFromattedTime(timeToFormat)
      console.log(`        _----_     `)
      console.log(`       /      )   < Hi, I was born in ${octopusBirthday}`)
      console.log(`      |@ @  _/     `)
      console.log(`  =_/%||;%)'       `)
      console.log(`,~=_%/;% |.~~_%~~  `)
      console.log(`   ~~_//  ⑊_-~~    `)
      console.log(`                   `)
      return this
  }
}
new Quiz().say()
